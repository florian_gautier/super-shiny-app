package my.company.app;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    /**
     * Another Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrueAgain()
    {
        assertTrue( true );
    }

        /**
     * Another Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithFalse()
    {
        assertFalse( false );
    }
}
